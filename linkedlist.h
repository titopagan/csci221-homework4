#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "node.h"

class LinkedList
{
public:
    LinkedList();
    void next();
    void reset();
    bool end();
    void *get_val();
    void insert_front(void *pval);
    void push_back(void *pval);
    void remove_at(int index);
    void *val_at(int index);
    void remove_duplicates(bool (*equal)(void*,void*));
    bool contains(bool (*equal)(void*,void*), void *pval);
    int length();
    LinkedList *copy();
	~LinkedList();
    
private:
    Node *node_at(int index);
    Node *head;
    Node *current;
    int count;
};

#endif
