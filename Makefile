CXX = g++
CXXFLAGS = -Wall -ansi -pedantic -ggdb3 -O0

all: testll

testll: testll.o linkedlist.o node.o
	$(CXX) $(CXXFLAGS) -o testll testll.o linkedlist.o node.o

testll.o: testll.cpp
	$(CXX) $(CXXFLAGS) -c testll.cpp

linkedlist.o: linkedlist.cpp linkedlist.h node.h
	$(CXX) $(CXXFLAGS) -c linkedlist.cpp

node.o: node.cpp node.h
	$(CXX) $(CXXFLAGS) -c node.cpp

.PHONY: clean
clean:
	rm -rf *.o testll
