#include <iostream>
#include <cassert>
#include "linkedlist.h"
using namespace std;

bool intsEqual(void *pval1, void *pval2)
{
    return (*((int*)pval1) == *((int*)pval2));
}

int main()
{
    LinkedList *list = new LinkedList;
    assert(list->length() == 0);

    int x0 = 5;
    list->push_back(&x0);
    assert(list->length() == 1);

    int x1 = 5;
    assert(true == list->contains(intsEqual, &x1));

    int x2 = 15;
    assert(false == list->contains(intsEqual, &x2));

	list->push_back(&x2);
	assert(true == list->contains(intsEqual, &x2));
	assert(list->length() == 2);
	assert(list->val_at(1) == &x2);
	
	int x3 = 22;
	list->insert_front(&x3);
	assert(true == list->contains(intsEqual, &x3));
	assert(list->length() == 3);
	assert(list->val_at(0) == &x3);
	
	list->remove_at(2);
	assert(list->length() == 2);
	assert(false == list->contains(intsEqual, &x2));
	
	int x4 = 54;
	int x5 = 54;
	list->insert_front(&x4);
	list->push_back(&x5);
	assert(list->length() == 4);
	assert(true == list->contains(intsEqual, &x4));
	list->remove_duplicates(intsEqual);
	assert(list->length() == 3);
	assert(list->val_at(0) == &x4);
    
	delete list;
	
	list = new LinkedList;
	
	list->insert_front((int*) 0);
	list->push_back((int*) 1);
	list->push_back((int*) 2);
	list->push_back((int*) 3);
	list->push_back((int*) 4);
	
	// example of walking through the list
	int i= 0;
    for(list->reset(); !list->end(); list->next())
    {
        void *pval = list->get_val();
		assert(pval == (int*)i);
		i++;
    }

	int j=0;
    for(int i = 0; i < list->length(); i++)
    {
        void *pval = list->val_at(j);
		assert(pval == (int*)j);
		j++;
    }

    cout << "End of main(). Success!" << endl;
	delete list;
}
