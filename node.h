#ifndef NODE_H
#define NODE_H

class Node
{
public:
    Node();
    void *get_val();
    void set_val(void *pval);
    Node *get_next();
    void set_next(Node *next);
	~Node();
private:
    void *pval;
    Node *next;
};

#endif
