#include <cstdlib> // for NULL constant
#include "node.h"

Node::Node()
{
    pval = NULL;
    next = NULL;
}

void *Node::get_val()
{
    return pval;
}

void Node::set_val(void *pval)
{
    this->pval = pval;
}

Node *Node::get_next()
{
    return next;
}

void Node::set_next(Node *next)
{
    this->next = next;
}

Node::~Node()
{
	next = NULL;
	delete pval;
}