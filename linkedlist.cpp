#include <cassert>
#include <cstdlib> // for NULL constant
#include <iostream>
#include "linkedlist.h"
#include "node.h"
using namespace std;

LinkedList::LinkedList()
{
    head = NULL;
    current = NULL;
    count = 0;
}

int LinkedList::length()
{
    return count;
}

void LinkedList::next()
{
    assert(!end());
    current = current->get_next();
}

void LinkedList::reset()
{
    current = head;
}

bool LinkedList::end()
{
    return (current == NULL);
}

void *LinkedList::get_val()
{
    assert(current != NULL);
    return current->get_val();
}

void *LinkedList::val_at(int index)
{
    assert(index >= 0 && index < count);
    return (node_at(index)->get_val());
}

void LinkedList::insert_front(void *pval)
{
    Node *n = new Node;
    n->set_val(pval);
    n->set_next(head);
    head = n;
    count++;
}

void LinkedList::push_back(void *pval)
{
    if(count == 0)
    {
        insert_front(pval);
    }
    else
    {
        Node *n = new Node;
        n->set_val(pval);
        Node *nlast = node_at(count - 1);
        nlast->set_next(n);
        count++;
    }
}

void LinkedList::remove_at(int index)
{
    // fails if index is bogus; also fails if count == 0, so we know
    // the list is not empty
    assert(index >= 0 && index < count);
    if(index == 0)
    {
        Node *ndelete = head;
        head = head->get_next(); // might be NULL; that's ok
        count--;
		
		delete ndelete;
    }
    else
    {
        Node *prev = node_at(index - 1);
        Node *ndelete = prev->get_next();
        prev->set_next(ndelete->get_next());
        count--;
		
		if (ndelete != NULL)
		{
			delete ndelete;
		}
    }
}

Node *LinkedList::node_at(int index)
{
    assert(index >= 0 && index < count);
    Node *n = head;
    for(int i = 0; i < index; i++)
    {
        n = n->get_next();
    }
    return n;
}

bool LinkedList::contains(bool (*equal)(void*, void*), void *pval)
{
    Node *pnode = head;
    while(pnode != NULL)
    {
        if((*equal)(pnode->get_val(), pval))
        {
            return true;
        }
        pnode = pnode->get_next();
    }
    return false;
}

void LinkedList::remove_duplicates(bool (*equal)(void*, void*))
{
    // temporary linked list containing vals already seen
    LinkedList *seen = new LinkedList;
    int i = 0;
    while(i < length()) // be sure length() is recomputed every time
    {
        void *pval = val_at(i);
        if(seen->contains(equal, pval))
        {
            remove_at(i);
            // don't increase i
        }
        else
        {
            seen->insert_front(pval);
            i++;
        }
    }
	
	delete seen;
}

LinkedList* LinkedList::copy()
{
    LinkedList *copy_list = new LinkedList;
    Node *pnode = head;
    while(pnode != NULL)
    {
        copy_list->push_back(pnode->get_val());
        pnode = pnode->get_next();
    }
    return copy_list;
}

LinkedList::~LinkedList()
{
	int size = length();
	for(int i = 0; i < size; i++)
	{
		remove_at(0);
	}	
	head = NULL;
	current = NULL;
	
}

